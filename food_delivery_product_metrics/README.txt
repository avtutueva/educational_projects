Analyze product metrics for an app

Stack: Python (pandas, plotly), product metrics

1. Calculate MAU for February
2. Calculate number of installs in January
3. Assign cohorts to users by the day the app was installed and calculate the conversion from install to purchase for them within 7 days. Which cohort had the highest CR?
4. Which paid marketing channel brought in the most new users?
5. Analyze at what stage of the funnel most of the customers fall off. See the scenarios for registered and unregistered users separately.
6. Users who came from which channels showed the lowest conversion on their first purchase?
7. Which paid acquisition channel (among ads) has the highest ROMI?